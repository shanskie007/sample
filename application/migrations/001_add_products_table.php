<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_products_table extends CI_Migration 
{
    public function up()
    {
        // Reference: https://www.codeigniter.com/user_guide/libraries/migration.html
        // Reference: https://www.codeigniter.com/user_guide/database/forge.html
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'name' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'image' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'price' => array(
                'type' => 'DOUBLE',
                'constraint' => '10,2',
            ),
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('tbl_products');

        $this->db->insert('tbl_products', [
            'name' => 'Samsung J2 Pro',
            'image' => '1.jpg',
            'price' => 100.00
        ]);
        $this->db->insert('tbl_products', [
            'name' => 'HP Notebook',
            'image' => '2.jpg',
            'price' => 299.00
        ]);

        $this->db->insert('tbl_products', [
            'name' => 'Panasonic T44 Lite',
            'image' => '3.jpg',
            'price' => 125.00
        ]);
    }

    public function down()
    {
        $this->dbforge->drop_table('tbl_products');
    }
}