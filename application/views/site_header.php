<!DOCTYPE html>
<html lang="en">
<head>
  <title>Sample Webpage</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="<?php echo base_url('resources/bootstrap/css/bootstrap.min.css');?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('resources/style.css');?>">
  <script src="<?php echo base_url('resources/bootstrap/js/angular.min.js');?>"></script>
</head>
<body>
