<div class="container" ng-app="shoppingCart" ng-controller="shoppingCartController" ng-init="loadProduct(); fetchCart();">
    <form method="post">
        <div class="row">
            <div class="col-md-3" style="margin-top:12px;" ng-repeat = "product in products">
                <div style="border:1px solid #333; background-color:#f1f1f1; border-radius:5px; padding:16px; height:350px;" align="center">
                    <img ng-src="/resources/images/{{product.image}}" class="img-responsive" /><br />
                    <h4 class="text-info">{{product.name}}</h4>
                    <h4 class="text-danger">{{product.price}}</h4>
                    <input type="button" name="add_to_cart" style="margin-top:5px;" class="btn btn-success form-control" value="Add to Cart" ng-click="addtoCart(product);" />
                </div>
            </div>
        </div>
    </form>
	<br />
    <h3 align="center">Your Cart Details</h3>
    <div class="table-responsive" id="order_table">
        <table class="table table-bordered table-striped">
            <tr>  
                <th width="40%">Product Name</th>  
                <th width="10%">Quantity</th>  
                <th width="20%">Price</th>  
                <th width="15%">Total</th>  
                <th width="5%">Action</th>  
            </tr>
            <tr ng-repeat = "cart in carts">
                <td>{{cart.product_name}}</td>
                <td>{{cart.product_quantity}}</td>
                <td>{{cart.product_price}}</td>
                <td>{{cart.product_quantity * cart.product_price}}</td>
                <td><button type="button" name="remove_product" class="btn btn-danger btn-xs" ng-click="removeItem(cart.product_id)">Remove</button></td>
            </tr>
            <tr>
                <td colspan="3" align="right">Total</td>
                <td colspan="2">{{ setTotals() }}</td>
            </tr>
        </table>
    </div>
</div>
<script>

var FETCH_ITEMS_URL = "<?php echo base_url('cart/fetchItems'); ?>";
var FETCH_CART_URL = "<?php echo base_url('cart/fetchCart'); ?>";
var ADD_CART_ITEM_URL = "<?php echo base_url('cart/addCartItem'); ?>";
var REMOVE_CART_ITEM_URL = "<?php echo base_url('cart/removeCartItem'); ?>";

var app = angular.module('shoppingCart', []);

app.controller('shoppingCartController', function($scope, $http){
	$scope.loadProduct = function(){
		$http.get(FETCH_ITEMS_URL).then(function(response){
            $scope.products = response.data;
        })
	};
	
	$scope.carts = [];
	
	$scope.fetchCart = function(){
		$http.get(FETCH_CART_URL).then(function(response){
            $scope.carts = response.data;
        })
	};
	
	$scope.setTotals = function(){
		var total = 0;
		for(var count = 0; count<$scope.carts.length; count++)
		{
			var item = $scope.carts[count];
			total = total + (item.product_quantity * item.product_price);
		}
		return total;
	};
	
	$scope.addtoCart = function(product){
		$http({
            method:"POST",
            url: ADD_CART_ITEM_URL,
            data: product
        }).then(function(data){
			$scope.fetchCart();
        });
	};
	
	$scope.removeItem = function(id){
		$http({
            method:"POST",
            url:REMOVE_CART_ITEM_URL,
            data:id
        }).then(function(data){
			$scope.fetchCart();
        });
	};
	
});

</script>