<?php
class User_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }
    public function add_product($product)
    {
        // Reference: https://www.codeigniter.com/user_guide/database/query_builder.html#inserting-data
        return $this->db->insert('entries', $product);
    }
    public function get_products($id = false)
    {
        // Reference: https://www.codeigniter.com/user_guide/database/results.html#id1
        // Reference: https://www.codeigniter.com/user_guide/database/query_builder.html#selecting-data
        return $this->db->get('tbl_products')->result();
    }

    public function get_shopping_cart()
    {
        // Reference: https://www.codeigniter.com/user_guide/libraries/sessions.html#retrieving-session-data
        return $this->session->userdata('shopping_cart');
    }

    public function add_cart_item()
    {
        $product_data = json_decode(file_get_contents("php://input"));
        if (!$product_data) {
            // Something went wrong
            return;
        }

        if (!isset($product_data->id) || !isset($product_data->name) || !isset($product_data->price)) {
            // Fields not required
            return;
        }

        $product_id = $product_data->id;
        $product_name = $product_data->name;
        $product_price = $product_data->price;
        $shopping_cart = $this->session->userdata('shopping_cart');

        if ($shopping_cart) {
            $is_available = 0;
            foreach ($shopping_cart as $keys => $values) {
                if (isset($shopping_cart[$keys]['product_id']) && $shopping_cart[$keys]['product_id'] == $product_id) {
                    $is_available++;
                    $shopping_cart[$keys]['product_quantity'] = $shopping_cart[$keys]['product_quantity'] + 1;
                }
            }
            if ($is_available == 0) {
                $item_array = array(
                    'product_id'               =>     $product_id,
                    'product_name'             =>     $product_name,
                    'product_price'            =>     $product_price,
                    'product_quantity'         =>     1
                );
                $shopping_cart[] = $item_array;
            }
        } else {
            $item_array = array(
                'product_id'               =>     $product_id,
                'product_name'             =>     $product_name,
                'product_price'            =>     $product_price,
                'product_quantity'         =>     1
            );
            $shopping_cart[] = $item_array;
        }
        $this->session->set_userdata('shopping_cart', $shopping_cart);
    }

    public function remove_cart_item()
    {
        $product_data = json_decode(file_get_contents("php://input"));
        $product_id = $product_data;
        $shopping_cart = $this->session->userdata('shopping_cart');

        if ($shopping_cart) {
            $has_removed = false;
            foreach ($this->session->shopping_cart as $keys => $values) {
                if (!isset($values["product_id"])) {
                    unset($shopping_cart[$keys]);
                }
                if (!isset($values["product_id"]) || (isset($values["product_id"]) && $values["product_id"] == $product_id)) {
                    unset($shopping_cart[$keys]);
                    $has_removed = true;
                }
            }
            if ($has_removed) {
                $this->session->set_userdata('shopping_cart', $shopping_cart);
            }
        }
    }
}
