<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migrate extends CI_Controller
{
	public function index()
	{
		// Reference: https://www.codeigniter.com/user_guide/libraries/migration.html
		$this->load->library('migration');
		if ($this->migration->current() === FALSE) {
			return show_error($this->migration->error_string());
		} else {
			return $this->responseJson([
				'message' => 'Success migration!'
			]);
		}
	}
	// Reference: https://www.codeigniter.com/user_guide/libraries/output.html
	private function responseJson($data, $code = 200)
	{
		return $this->output
			->set_content_type('application/json')
			->set_status_header($code)
            ->set_output(json_encode($data));
	}
}
