<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Cart extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
		$this->load->model('user_model');
		$this->load->library('session');
    }

    public function index()
    {
        $this->load->view('site_header');
        $this->load->view('cart');
        $this->load->view('site_footer');
    }

    public function fetchItems()
    {
        return $this->responseJson($this->user_model->get_products());
    }

	public function fetchCart()
	{
		$shopping_cart = $this->user_model->get_shopping_cart();
        return $this->responseJson($shopping_cart);
	}
	
	public function addCartItem()
	{
		$this->user_model->add_cart_item();
        return $this->responseJson([
			'message' => 'Successfully added to cart!'
		]);
	}

	public function removeCartItem()
	{
		$this->user_model->remove_cart_item();
        return $this->responseJson([
			'message' => 'Successfully removed item from the cart!'
		]);
	}

    private function responseJson($data, $code = 200)
    {
        return $this->output
            ->set_content_type('application/json')
            ->set_status_header($code)
            ->set_output(json_encode($data));
    }
}
